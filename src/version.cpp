#include <Rcpp.h>
#include <leveldb/db.h>

// [[Rcpp::export]]
std::string leveldb_version() {
    std::ostringstream res;
    res << leveldb::kMajorVersion << "." << leveldb::kMinorVersion;
    return res.str();
}
