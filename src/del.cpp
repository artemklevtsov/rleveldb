#include <Rcpp.h>
#include <leveldb/db.h>
#include <leveldb/write_batch.h>

// [[Rcpp::export]]
bool db_mdel(SEXP db, Rcpp::StringVector keys) {
    Rcpp::XPtr<leveldb::DB> ptr(db);
    
    std::size_t n = keys.size();
    leveldb::WriteBatch batch;
    for (std::size_t i = 0; i < n; ++i) {
        batch.Delete(leveldb::Slice(keys[i]));
    }
    
    leveldb::WriteOptions opts;
    opts.sync = true;
    
    leveldb::Status status = ptr->Write(opts, &batch);
    if (!status.ok()) {
        Rcpp::stop("Can't write to db. ", status.ToString());
    }
    
    return true;
}

