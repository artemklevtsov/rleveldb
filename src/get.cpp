#include <Rcpp.h>
#include <leveldb/db.h>

// [[Rcpp::export]]
Rcpp::CharacterVector db_mget(SEXP db, Rcpp::StringVector keys) {
    Rcpp::XPtr<leveldb::DB> ptr(db);
    
    std::size_t n = keys.size();
    
    Rcpp::StringVector res(Rcpp::no_init(n));
    for (std::size_t i = 0; i < n; ++i) {
        std::string value;
        leveldb::Slice key(keys[i]);
        leveldb::ReadOptions opts;
        leveldb::Status status = ptr->Get(opts, key, &value);
        if (status.ok()) {
            res[i] = value;
        } else {
            // Rcpp::warning(status.ToString());
            res[i] = NA_STRING;
        }
    }
    
    return res;
}
