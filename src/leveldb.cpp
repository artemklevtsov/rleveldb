#include <Rcpp.h>
#include <leveldb/db.h>
#include <leveldb/write_batch.h>

// [[Rcpp::export]]
SEXP db_con(const std::string& path) {
  leveldb::Options opts;
  opts.create_if_missing = true;
  leveldb::DB *db;
  leveldb::Status status = leveldb::DB::Open(opts, path, &db);
  if (!status.ok()) {
    Rcpp::stop("Failed to open db. ", status.ToString());
  }
  Rcpp::XPtr<leveldb::DB> ptr(db);
  return ptr;
}

// [[Rcpp::export]]
bool db_dcon(SEXP db) {
  Rcpp::XPtr<leveldb::DB> ptr(db);
  ptr.release();
  return true;
}

// [[Rcpp::export]]
bool db_destroy(const std::string& path) {
  leveldb::Options opts;
  leveldb::Status status = leveldb::DestroyDB(path, opts);
  if (!status.ok()) {
    Rcpp::stop("Can't destroy db. ", status.ToString());
  }

  return true;
}

// [[Rcpp::export]]
Rcpp::StringVector db_keys(SEXP db) {
  Rcpp::XPtr<leveldb::DB> ptr(db);

  Rcpp::StringVector res;
  leveldb::ReadOptions opts;
  leveldb::Iterator* it = ptr->NewIterator(opts);
  for (it->SeekToFirst(); it->Valid(); it->Next()) {
    res.push_back(it->key().ToString());
  }

  if (!it->status().ok()) {
    Rcpp::stop(it->status().ToString());
  }

  return res;
}
