#include <Rcpp.h>
#include <leveldb/db.h>

// [[Rcpp::export]]
Rcpp::StringVector db_to_vec(SEXP db) {
    Rcpp::XPtr<leveldb::DB> ptr(db);
    
    Rcpp::StringVector keys;
    Rcpp::StringVector values;
    
    leveldb::ReadOptions opts;
    leveldb::Iterator* it = ptr->NewIterator(opts);
    for (it->SeekToFirst(); it->Valid(); it->Next()) {
        keys.push_back(it->key().ToString());
        values.push_back(it->value().ToString());
    }
    
    if (!it->status().ok()) {
        Rcpp::stop(it->status().ToString());
    }
    
    values.names() = keys;
    return values;
} 
