#include <Rcpp.h>
#include <leveldb/db.h>

// [[Rcpp::export]]
Rcpp::LogicalVector db_mexists(SEXP db, Rcpp::StringVector keys) {
    Rcpp::XPtr<leveldb::DB> ptr(db);
    
    std::size_t n = keys.size();
    Rcpp::LogicalVector res(Rcpp::no_init(n));
    
    leveldb::ReadOptions opts;
    leveldb::Iterator* it = ptr->NewIterator(opts);
    for (std::size_t i = 0; i < n; ++i) {
        leveldb::Slice key(keys[i]);
        it->Seek(key);
        res[i] = it->Valid();
    }
    
    if (!it->status().ok()) {
        Rcpp::stop(it->status().ToString());
    }
    
    return res;
}
